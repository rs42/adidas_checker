#!/usr/bin/env python3.5
import requests
import json
import time

sku = input("Enter Product ID: ").upper()

base_url = 'https://www.adidas.ru/api/inventory-check?isCnCRestricted=false&lat=55.755826&lng=37.617299900000035&sku='#EG6489_420
headers = {"user-agent" : "Mozilla/5.0 (X11; Fedora; Linux x86_64) \
        AppleWebKit/537.36 (KHTML, like Gecko) Chrome/78.0.3904.108 \
        Safari/537.36", "accept-language" : "en-US,en;q=0.9"}
def getRUsize(size):
    return 35.5 + (size - 530) * 0.05

def getVarientStock(sku):
    print("Moscow offline stock: " + sku)
    #35.5 - 48 RU
    for size in range(530, 740, 10):
        urlVariantStock = base_url + sku + '_' + str(size)
        #print("Trying " + urlVariantStock)
        r = requests.get(urlVariantStock, headers=headers)
        try:
           # print(r.text)
            stock_stores = json.loads(r.text)['filteredStores']
        except:
            if r.status_code == 404:
                print ("ERROR! " + sku + '_' + str(size) + '" is an invalid PID!')
            elif r.status_code == 403:
                print ("ERROR! IP banned!")
            else:
                print("ERROR! Stock check failed!")
        
        if not stock_stores:
            time.sleep(5)
            continue
        print("----------------------------")
        print(sku + " size: " + str(getRUsize(size)) + " RU")

        for store in stock_stores:
            if (store["availability"] == "now"):
                print("\n" + store["availability"].upper() + " " + \
                    store["name"] + " " +  store["street"] + "\n")
            else:
                print(store["availability"] + " " + store["name"] + " " + store["street"])

        print("----------------------------")
        time.sleep(5)

getVarientStock(sku)
